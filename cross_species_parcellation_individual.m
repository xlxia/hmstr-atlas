clear;clc;
addpath(genpath('/usr/local/MATLAB/packages'));
MAX_CLUSTERS=10;
LR='LH';
ROI_name='LH_str';
subs=textread('list.txt','%s');
for subn=1:length(subs)
	clear ind hFC mFC FC iFC
	sub=subs{subn}; %% e.g., 101107-sub-032193
	hsub=sub(1:6); 
	msub=sub(8:length(sub));

	% load images/matrixes
        disp('load human images & matrixes...')
	tmp=load(strcat('/media/xlxia/',hsub,'/func/',ROI_name,'/',ROI_name,'_NO2eta2_corr2.mat'));
	him=tmp.eta2;
    info=load_untouch_nii(strcat('/media/xlxia/',hsub,'/func/',ROI_name,'/',ROI_name,'_NO2eta2_2clusters.nii.gz'));
	hROI=info.img;
    disp('load macaque images & matrixes...') %% ../reg/.. magnified_macaque_striatum
    mfiles=dir(strcat('/home/xlxia/Macaques/site-uwo/',msub,'/ses-001/func/',ROI_name,'/reg/*gz'));
    info=load_untouch_nii(strcat(mfiles(1).folder,'/',mfiles(1).name));
    mROI2h=info.img;
    
    % get overlapped data.
    ind=find(hROI>0);
    [x,y,z]=ind2sub(size(hROI),ind);
    op=zeros(1,length(x));
    for i=1:length(x)
        if mROI2h(x(i),y(i),z(i))>0
            op(1,i)=1;
        end
    end
    him=him(:,op==1);
    
    tmp=(hROI>0).*(mROI2h>0);
    ind=find(tmp==1);
    [x,y,z]=ind2sub(size(hROI),ind);
    mim=zeros(length(mfiles),length(x));
    for num=1:length(mfiles)
        info=load_untouch_nii(strcat(mfiles(num).folder,'/',mfiles(num).name));
        mROI2h=info.img;
        for i=1:length(x)
            mim(num,i)=mROI2h(x(i),y(i),z(i));
        end
        
    end
    im=[him;mim];
    
    % calcuate FC
	FC=zeros(size(im,1),size(im,1));
	disp('calculate FC:')
	for i=1:size(im,1)
            for j=1:size(im,1)
                FC(i,j)=corr(im(i,:)',im(j,:)'); 
            end
        end        
    
    FC(isnan(FC))=0;
    cd(strcat('/media/xlxia/cross-species-atlas-db2/',sub,'/',ROI_name))
	save NO2_corr_1_600.mat FC
	
	% calcuate eta2
	corrData=FC;
	mn=mean(corrData(:)); 
	sd=std2(corrData(:));
	corrData=(corrData-mn)/sd;
	eta2=zeros(size(corrData,1),size(corrData,1));

	nnmat=ones(size(corrData,1));
	nmat=ones(size(corrData,1),1);
	lmat=ones(size(corrData,2),1);
	[n,l]=size(corrData);

	T=kron(transpose(nmat),((corrData.*corrData)*lmat));
	TTU=(T-2*corrData*transpose(corrData)+transpose(T))/2;
	M=(l/2)*(kron(transpose(nmat),((1/l)*corrData*lmat).^2));
	TTD=T+transpose(T)-M-transpose(M)-((1/l)*corrData*lmat)*transpose(corrData*lmat);
	eta2=nnmat-(TTU./TTD);
	eta2=tril(eta2,-1);
	eta2=eta2+eta2';
	eta2=eta2+eye(size(eta2));
	iFC=eta2;
	if strcmp(ROI_name,'LH_thr')
	    save NO2eta2_1_600.mat iFC
	elseif strcmp(ROI_name,'RH_thr')
	    save NO2eta2_1_600.mat iFC
	end
	
	cd('/media/xlxia/cross-species-atlas-db2/')
	iFC=iFC-diag(diag(iFC));
	hinfo=load_untouch_nii(strcat('/media/xlxia/',hsub,'/func/',ROI_name,'/',ROI_name,'_NO2eta2_2clusters.nii.gz'));
    ind=find(hinfo.img>0);
    [hx,hy,hz]=ind2sub(size(hinfo.img),ind);
	minfo=load_untouch_nii(strcat('/home/xlxia/Macaques/site-uwo/',msub,'/ses-001/func/',ROI_name,'/',ROI_name,'.nii.gz'));
    ind=find(minfo.img>0);
    [mx,my,mz]=ind2sub(size(minfo.img),ind);
    if (length(hx)==size(him,1)) && (length(mx)==size(mim,1))
        for k=2:MAX_CLUSTERS
            index=sc3(k,iFC);         
            clear image_h image_m
            hindex=index(1:size(him,1)); 
            image_h=zeros(size(hROI));
            for i=1:length(hx)
                image_h(hx(i),hy(i),hz(i))=int8(hindex(i));
            end
            hinfo.img=image_h;
            save_untouch_nii(hinfo,strcat('/media/xlxia/cross-species-atlas-db2/',sub,'/',ROI_name,'/human_',num2str(k),'clusters.nii.gz')); 

            mindex=index((length(hx)+1):(length(hx)+length(mx)));
            image_m=zeros(size(minfo.img));
            for i=1:length(mx)
                image_m(mx(i),my(i),mz(i))=int8(mindex(i));
            end
            minfo.img=image_m;
            save_untouch_nii(minfo,strcat('/media/xlxia/cross-species-atlas-db2/',sub,'/',ROI_name,'/macaque_',num2str(k),'clusters.nii.gz'));
        end
    end
end
