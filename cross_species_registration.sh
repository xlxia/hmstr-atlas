# macaque_striatum: registration from monkey civm (0.15 mm3) to civm_template (0.5 mm3) space
flirt -in /usr/local/template_atlas/monkey/civm/civm_template.nii.gz -ref /usr/local/template_atlas/monkey/civm/civm_rhesus_v1_b0.nii.gz -omat template2b0.mat
convert_xfm -omat b02template.mat -inverse template2b0.mat
flirt -in macaque_striatum.nii.gz -ref /usr/local/template_atlas/monkey/civm/civm_template.nii.gz -out macaque_striatum_temp.nii.gz -applyxfm -init b02template.mat

# reslice & magnified macaque striatum
matlab -nodisplay -nosplash -r "magnified_reslice("macaque_striatum_temp.nii.gz");exit;"

# local-brain registration: magnified_macaque_striatum => human_striatum
antsRegistrationSyNQuick.sh -d 3 -m magnified_macaque_striatum_temp.nii.gz -f human_striatum.nii.gz -o m2h
antsApplyTransforms -d 3 -i magnified_macaque_striatum_temp.nii.gz -r human_striatum.nii.gz -o magnified_macaque_striatum_temp_2human_striatum.nii.gz -t m2h1Warp.nii.gz -t m2h0GenericAffine.mat
