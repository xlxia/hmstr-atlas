clear;clc;
addpath(genpath('/usr/local/MATLAB/packages'));
ROI_name='RH_str';LR='RH';
subs=textread('list.txt','%s');
for sn=1:length(subs)
    sub=subs{sn}; 
    cd(strcat('/media/xlxia/',sub,'/func/',ROI_name))
	if ~exist(strcat('/media/xlxia/xxl3/',sub,'/func/',ROI_name,'/',ROI_name,'_NO2eta2_1_600_corr2.nii.gz'))
    
    info=load_untouch_nii(strcat('/media/xlxia/xxl3/',sub,'/func/',ROI_name,'/',ROI_name,'.nii.gz'));
    ROI=info.img;
    ind=find(ROI>0);
    [x,y,z]=ind2sub(size(ROI),ind);
    
    temp=load(strcat(ROI_name,'_eta2_1_600.mat'));
    iFC=temp.iFC;
    dFC=zeros(size(iFC));
    for i=1:size(dFC,1)
        dFC(i,i)=1;
        for j=i:size(dFC,1)
            dFC(i,j)=corr(iFC(i,:)',iFC(j,:)');
            dFC(j,i)=dFC(i,j);
        end
    end
    
    corrData=dFC;
    mn=mean(corrData(:)); 
    sd=std2(corrData(:));
    corrData=(corrData-mn)/sd;
    eta2=zeros(size(corrData,1),size(corrData,1));

    nnmat=ones(size(corrData,1));
    nmat=ones(size(corrData,1),1);
    lmat=ones(size(corrData,2),1);
    [n,l]=size(corrData);

    T=kron(transpose(nmat),((corrData.*corrData)*lmat));
    TTU=(T-2*corrData*transpose(corrData)+transpose(T))/2;
    M=(l/2)*(kron(transpose(nmat),((1/l)*corrData*lmat).^2));
    TTD=T+transpose(T)-M-transpose(M)-((1/l)*corrData*lmat)*transpose(corrData*lmat);
    eta2=nnmat-(TTU./TTD);
    eta2=tril(eta2,-1);
    eta2=eta2+eta2';
    eta2=eta2+eye(size(eta2));
    if strcmp(ROI_name,'LH_str')
        save LH_str_NO2corr_1_600_corr2.mat dFC
        save LH_str_NO2eta2_1_600_corr2.mat eta2
    elseif strcmp(ROI_name,'RH_str')
        save RH_str_NO2corr_1_600_corr2.mat dFC
        save RH_str_NO2eta2_1_600_corr2.mat eta2
    end
    
	cd('/media/xlxia/');
	MAX_CLUSTERS=20;
	for k=2:MAX_CLUSTERS
		index=sc3(k,eta2);
		image_f=zeros(size(ROI));
		for i=1:length(x)
			image_f(x(i),y(i),z(i))=int8(index(i));
		end
		info.img=image_f;
		save_untouch_nii(info,strcat(sub,'/func/',ROI_name,'/',ROI_name,'_NO2eta2_',num2str(k),'clusters.nii.gz'));
    end
    end
end


